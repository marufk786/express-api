const express = require('express');
const router = express.Router();
// controller
const {signup, signin, signout} = require('../controllers/authController')

// validators
const {createUserValidator, signinValidator} = require('../validators/');

// routes
// signup and signin routes
router.post('/signup', createUserValidator,signup);
router.post('/signin',signinValidator,signin );
// signout 
router.get("/signout", signout)



module.exports = router