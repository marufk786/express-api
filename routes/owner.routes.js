const express = require('express')
const router = express.Router();

// controllers 
const {ownerSignup, ownerSignin,ownerSignout, requireSignin} = require('../controllers/ownerController');

// validators 
const {ownerSignupValidator} = require('../validators/');


// routes 
router.post('/owner-signup', ownerSignupValidator ,ownerSignup )
router.post('/owner-signin', ownerSignin)
// logout / signout 
router.get('/owner-signout',requireSignin, ownerSignout)

module.exports = router;