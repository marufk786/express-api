const express = require('express')
const {requireSignin} = require('../controllers/authController')
const router = express.Router()

// Validators / Middleware 
const {createPostValidator} = require('../validators/');

// Controllers 
const {getPosts, createPost} = require('../controllers/postController')



router.get('/', requireSignin ,getPosts);
router.post('/post', requireSignin,createPostValidator, createPost);




module.exports = router;