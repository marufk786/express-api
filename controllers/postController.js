const Post = require('../models/post.model')


exports.getPosts = (req, res) => {
   const posts = Post.find()
   .select("_id title body")
   .then(posts=> {
       res.status(200).json({
           data:posts
       })
   })
   .catch(error=> {
       console.log(error)
       res.status(500).json({
           error: error
       })
   })
    
}
      

                
 exports.createPost = (req,res)=>{
    // inintializing post
    const post = new Post({
        title: req.body.title,
        body: req.body.body
    });
    
    // saving post to database
    post.save().then(result=>{
        res.status(201).json({
            post:result
        });
    });

    
}











