const jwt = require('jsonwebtoken');
require('dotenv').config();
const User = require('../models/userModel');
const expressJWT = require('express-jwt')

// signup
exports.signup = async (req,res)=>{

    // first check whether the user already exists  
    const userExists = await User.findOne({email: req.body.email})
    if(userExists){
        return res.status(403).json({
            error: "Email is taken"
        });
    }

    // if user doesn't exists then signup and saving in database
    const user = await new User(req.body);
    await user.save();
    
    // sending response
    res.status(201).json({
        msg:'Signup success. Please login.',
        user
    })

}


// signin method
exports.signin = (req, res) =>{

    // find the user based on email 
    const {email,password} = req.body  

    User.findOne({email},(err,user)=>{
        // if err or no user
        if(err || !user){
            return res.status(401).json({
                error:"User not found."
            })
        }

        // if user is found make sure the email and password match
        // create a authenticate method in model and user here
        if(!user.authenticate(password)){
            return res.status(401).json({
                error:"Wrong Email or Password"
            });
        }

        
        // generate a token with user id and secret 
        const token = jwt.sign({_id: user._id}, process.env.JWT_SECRET);

        // persist the token as 't' in cookie with expiry date

        res.cookie('t',token, {expire: new Date() + 9999})

        // return response with user and token to forntend client 

        const {_id,name,email} = user;

        return res.json({
            token,
            user: {_id,name,email}
        })
    });
}


// signout 
exports.signout = (req,res)=>{
    res.clearCookie("t")
    return res.json({
        message:"Signout success!"
    });
};

exports.requireSignin = expressJWT({
    secret : process.env.JWT_SECRET,
    algorithms: ['sha1', 'RS256', 'HS256']
})
