const jwt = require('jsonwebtoken');
require('dotenv').config();
const expressJWT = require('express-jwt');
const Owner = require('../models/OwnerModel');

// Owner Signup 
exports.ownerSignup = async (req,res) =>{
    
    // first check if the user exists 
    const ownerExists = await Owner.findOne({email: req.body.email})
    if(ownerExists){
        return res.status(403).json({
            error: "Email is taken. Choose a different email"
        });
    }

    // if owner doesn't exits, then signup and save in database
    const owner = await new Owner(req.body);
    await owner.save();

    // sending response 
    res.status(201).json({
        msg:"Signup success! Please Signin",
        owner
    })



}


// Owner Sign in
exports.ownerSignin = (req,res)=>{

    // clear authentication cookies if available
    if(res.cookie("owner")){
        res.clearCookie("owner")
    }

    if(res.cookie("t")){
        res.clearCookie("t")
    }

    const {email, password} = req.body;

    Owner.findOne({email},(err, owner)=>{

        if(err || !owner){
            return res.status(401).json({
                error : "Owner not found"
            })
        }

        // if owner found, then check password
        if(!owner.authenticate(password)){
            return res.status(401).json({
                msg:"Wrong Email or Password Combination"
            });
        } 

        // generate token for owner 
        const token = jwt.sign({_id:owner._id},process.env.JWT_SECRET)

        // persist the token as owner in the cookie with expiry date 
        res.cookie('owner',token,{expire: new Date()+999})

        // returning response to frontend with token and owner details
        const {_id,firstName,email} = owner;
       

        return res.status(200).json({
            token,
            owner:{_id, firstName, email}
        })


    })
}

// Owner Signout 
exports.ownerSignout = (req,res)=>{
    if(res.cookie('t')){
        res.clearCookie('t')
    }
    res.clearCookie("owner")

    return res.json({
        msg:"Owner Signout successfull"
    })


};

// signin required method 
exports.requireSignin = expressJWT({
    secret: process.env.JWT_SECRET,
    algorithms: ['HS256'] 
})