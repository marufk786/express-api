const mongoose = require('mongoose')

// Post Schema
const postSchema = new mongoose.Schema({
    title:{
        type: String,
        required:true
       
    },
    body:{
        type: String,
        required:true,
    }
});

module.exports = mongoose.model("Post", postSchema)