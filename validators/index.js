const {body} = require('express-validator')

// create post validator 
exports.createPostValidator = (req,res,next) => {
    /**** title */
    // check whether the title is empty or not 
    req.check('title',"Title can not be empty").notEmpty();

    // checking length of title
   req.check('title','Title must be between 4 to 150 characters').isLength({
       min:4,
       max:150
   });
       
    
    /***** Body */
    // checking if body is empty
    req.check('body','Body can not be empty').notEmpty();

    // checking body length 
    req.check('body', 'Body length must be 4 to 1000 characters').isLength({
        min:4,
        max: 2000
    });
    
    
    // checking for errors 
    const errors = req.validationErrors();

    // if error, show the  first one as happen
    if(errors){
        const firstError = errors.map(error => error.msg)[0];
        return res.status(400).json({error: firstError})
    }
    
    // proceed to the next middleware
    next();


}


// create user validator 
exports.createUserValidator = (req,res,next) =>{
    /*** Name */
    // check if name is empty
    req.check('name','Name can not be empty').notEmpty();

    // check length of name
    req.check('name','Name must be between 4 to 20 characters').isLength({
        min:2,
        max:20
    });

    // check email
    req.check('email','Email can not be empty').notEmpty();

    req.check('email','Must be a valid email address').isEmail();
    
    req.check('email','Can not be less than 3 characters').isLength({
        min:3
    });

    // check password
    req.check('password','Password must contain at least 6 characters').isLength({
        min:6
    })
    
    req.check('password','Password must contain at a number').matches(/\d/)
   

    // checking for error messages
    const errors = req.validationErrors()
    
    if(errors){
        const firstError = errors.map(error => error.msg)[0];
        return res.status(400).json({
            error : firstError
        });
    }
    // proceed to next middleware
    next();
}

// sigin validator 
exports.signinValidator = (req,res,next)=>{
    // check email 
    req.check('email','Must be a valid email address').isEmail()


    // checking for error message 
    const errors = req.validationErrors()

    if(errors){
        const firstError = errors.map(error => error.msg)[0];
        return  res.status(400).json({
            error: firstError
        });
    }

    next();
}

// Owner signup validation 
exports.ownerSignupValidator = (req,res,next)=>{
    // firstName check 
    req.check('firstName','First Name can not be empty').notEmpty();
    req.check('firstName','First Name must be between 3 to 20 characters').isLength({
        min:3,
        max:20
    })
   
    // lastName check 
    req.check('lastName','Last Name can not be empty').notEmpty();
    req.check('lastName','Last Name must be between 3 to 20 charcters').isLength({
        min:3,
        max:20
    });

    // email check
    req.check('email','Email can not be empty').notEmpty();
    req.check('email', 'Must be a valid email address').isEmail();

    // password check 
    req.check('password','Password can not be empty').notEmpty();
    req.check('password','Password must be more than 6 charcters').isLength({
        min: 6
    });
    req.check('password','Password should have at a number').matches(/\d/)

    // contact check 
    req.check('contactNumber','Contact number can not be empty').notEmpty();
    req.check('contactNumber','Must be a valid phone number').matches(/^[\+]?([0-9][\s]?|[0-9]?)([(][0-9]{3}[)][\s]?|[0-9]{3}[-\s\.]?)[0-9]{3}[-\s\.]?[0-9]{4,6}$/im
    )

    // checking for errors 
    const errors = req.validationErrors();
    
    if(errors){
        const firstError = errors.map(error => error.msg)[0];
        return res.status(400).json({
            error : firstError
        });
    }

    // proceed to next middleware
    next();

}



