const express = require('express')
const app = express();
const mongoose = require('mongoose')
const morgan = require('morgan')
const dotenv = require('dotenv')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const connectDB = require('./db')
const expressValidator = require('express-validator')
dotenv.config()


// db 
connectDB()


// middleware
app.use(express.json())
app.use(cookieParser())
app.use(bodyParser.urlencoded())
app.use(bodyParser.urlencoded({
    extended: true
  }));
app.use(bodyParser.json())
app.use(morgan('dev'))
app.use(expressValidator())


// routes imports
// const {getPosts} = require('./routes/post.routes')
const postRoutes = require('./routes/post');
const authRoutes = require('./routes/auth.routes')
const ownerRoutes = require('./routes/owner.routes');



// routes
app.use('/',postRoutes)
app.use('/', authRoutes)
app.use('/', ownerRoutes)

// handling unauthorized error
app.use(function(err, req, res, next){
    if (err.name === 'UnauthorizedError')
    {
        res.status(401).json({
            error: "Unauthorized Error",
            msg:"Please login with valid credentials"
        });
    }
});





const PORT = process.env.PORT || 5000;

app.listen(PORT, ()=> console.log(`Server is running on port ${PORT}`))